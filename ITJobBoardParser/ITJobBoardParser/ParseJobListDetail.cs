﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HtmlAgilityPack;
using System.Net;
using System.IO;
using System.Drawing;

namespace ITJobBoardParser
{

    public class ParseJobListDetail
    {
        string url;
        HtmlDocument doc = new HtmlDocument();

        public string HTML
        {
            get
            {
                return doc.DocumentNode.OuterHtml;
            }
        }

        public ParseJobListDetail(string _url)
        {
            url = _url;
            Uri targetUri = new Uri(url);
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(targetUri);
            var response = request.GetResponse() as HttpWebResponse;
            StreamReader reader = new StreamReader(response.GetResponseStream());
            doc.LoadHtml(reader.ReadToEnd());
        }

        public Job Parse()
        {
            Job j = new Job();
            HtmlNode node = doc.GetElementbyId("ContentWrapper");
            HtmlNode logo = doc.GetElementbyId("ClientLogo");
            Uri targetUri = new Uri(url);
            j.ID = targetUri.Segments[targetUri.Segments.Count() - 2].TrimEnd("/".ToCharArray());

            if (node != null)
            {
                var items = from n in node.Descendants() where n.Attributes.Contains("itemprop") select n;
                foreach (var item in items)
                {
                    if (item.Attributes["itemprop"].Value == "industry")
                    {
                        j.Industry = item.InnerText;
                    }
                    else if (item.Attributes["itemprop"].Value == "title")
                    {
                        j.Title = item.InnerText;
                    }
                    else if (item.Attributes["itemprop"].Value == "description")
                    {
                        j.Description = item.InnerHtml;
                    }
                    else if (item.Attributes["itemprop"].Value.ToLower() == "basesalary")
                    {
                        j.Salary = item.InnerText;
                    }
                    else if (item.Attributes["itemprop"].Value.ToLower() == "addressregion")
                    {
                        j.Location = item.InnerText;
                    }
                    else if (item.Attributes["itemprop"].Value.ToLower() == "employmenttype")
                    {
                        j.Jobtype = item.InnerText;
                    }
                }
                
                foreach (HtmlNode n in doc.DocumentNode.SelectNodes("//table[@itemprop='hiringOrganization']//tr"))
                {
                    HtmlNodeCollection coll = n.SelectNodes("td");
                    if (coll.Count == 2)
                    {
                        if (coll[0].InnerText.ToLower() == "advertiser")
                        {
                            j.CompanyName = coll[1].InnerText;
                        }
                        else if (coll[0].InnerText.ToLower() == "contact name")
                        {
                            j.ContactName = coll[1].InnerText;
                        }
                        else if (coll[0].InnerText.ToLower() == "telephone")
                        {
                            j.Telephone = coll[1].SelectSingleNode("//span[@itemprop='telephone']").InnerText;
                        }
                        else if (coll[0].InnerText.ToLower() == "email")
                        {
                            j.Email = coll[1].SelectSingleNode("//span[@itemprop='email']").InnerText;
                        }
                        else if (coll[0].InnerText.ToLower() == "reference")
                        {
                            j.Reference = coll[1].SelectSingleNode("//span[@id='ClientAdvertRef']").InnerText;
                        }
                    }
                }

                if (logo != null)
                {
                    if (logo.Attributes["src"] != null)
                    {
                        j.EmployerType = 1;
                        Uri logourl = new Uri(logo.Attributes["src"].Value);
                        string ext = Path.GetExtension(string.Format("D:\\{0}", logourl.Segments[logourl.Segments.Count() - 1]));
                        string path = string.Format("~/company-logo/{0}{1}", j.CompanyName.Trim().Replace(" ", "-"), ext);
                        if (!File.Exists(HttpContext.Current.Server.MapPath(path)))
                        {
                            DownloadImageFromUrl(logo.Attributes["src"].Value).Save(HttpContext.Current.Server.MapPath(path));
                            j.CompanyLogo = path;
                        }
                        else
                        {
                            j.CompanyLogo = path;
                        }
                    }
                    else
                    {
                        j.EmployerType = 2;
                        j.CompanyLogo = string.Empty;
                    }
                }
                else
                {
                    j.EmployerType = 2;
                    j.CompanyLogo = string.Empty;
                }

                if (doc.GetElementbyId("OriginalPostedDate") != null)
                {
                    j.PostDate = DateTime.Parse(doc.GetElementbyId("OriginalPostedDate").InnerText).ToString();
                }
            }
            return j;
        }

        public Image DownloadImageFromUrl(string imageUrl)
        {
            Image image = null;

            try
            {
                System.Net.HttpWebRequest webRequest = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(imageUrl);
                webRequest.AllowWriteStreamBuffering = true;
                webRequest.Timeout = 20000;

                System.Net.WebResponse webResponse = webRequest.GetResponse();

                System.IO.Stream stream = webResponse.GetResponseStream();

                image = Image.FromStream(stream);

                webResponse.Close();
            }
            catch (Exception)
            {
                return null;
            }

            return image;
        }
    }
}
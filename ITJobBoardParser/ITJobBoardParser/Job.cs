﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITJobBoardParser
{
    public class Job
    {
        public string ID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string CompanyName { get; set; }
        public string CompanyLogo { get; set; }
        public string ContactName { get; set; }
        public string Telephone { get; set; }
        public string Reference { get; set; }
        public string Salary { get; set; }
        public string MinSalary { get; set; }
        public string MaxSalary { get; set; }
        public bool PerAnnum { get; set; }
        public bool PerMonth { get; set; }
        public string Jobtype { get; set; }
        public string Location { get; set; }
        public string PostDate { get; set; }
        public string Email { get; set; }
        public string Industry { get; set; }
        public byte EmployerType { get; set; }
        public string Link { get; set; }
    //Testing1 Push this to Test  
    // This is the second push
    }
}

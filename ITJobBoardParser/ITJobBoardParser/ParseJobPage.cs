﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITJobBoardParser
{
    public class ParseJobPage
    {


        public bool Parse(int Page) 
        {
            List<Job> ListofJobs = new List<Job>();
            ParseJobList jlp = new ParseJobList(BuildPageURL(Page));
            ListofJobs.AddRange(jlp.Parse());
            return true;
        }

        public string BuildPageURL(int Page) 
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Job List Page - " + Page);
            sb.Append("<br/>");
            string JobListURL = "http://www.theitjobboard.co.uk/IT-Jobs/all-jobs/all-locations/all/0/relevance/en/?source=Search&Currency=GBP&RadiusUnit=2&Page=" + Page;
            sb.Append("Job List URL - <br/>");
            sb.Append(JobListURL);
            sb.Append("<br/>");
            return sb.ToString();
        }


    
    
    }
}

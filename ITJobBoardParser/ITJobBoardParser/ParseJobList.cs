﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Net;
using HtmlAgilityPack;
using System.IO;
using System.Text.RegularExpressions;

namespace ITJobBoardParser
{
     

    public class ParseJobList
    {
        string url;
        HtmlDocument doc = new HtmlDocument();

        public int PageIndex { get; set; }
        public bool NextPage { get; set; }

        public ParseJobList(string _url)
        {
            PageIndex = 1;
            url = _url;
            Load();
        }

        public void Load()
        {
            Uri targetUri = new Uri(url);
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(targetUri);
            var response = request.GetResponse() as HttpWebResponse;
            StreamReader reader = new StreamReader(response.GetResponseStream());
            doc.LoadHtml(reader.ReadToEnd());
        }
        public List<Job> Parse()
        {
            List<Job> links = new List<Job>();

            var items = doc.DocumentNode.SelectNodes("//a[@itemprop='url']"); //from n in doc.DocumentNode.Descendants() where n.Attributes.Contains("itemprop") select n;

            foreach (var item in items)
            {
                if (item.Attributes["href"] != null)
                {
                    string url = item.Attributes["href"].Value;
                    Uri u = new Uri(url);
                    links.Add(new Job() { Link = url.Replace(u.Query, ""), Detail = null });
                }
            }

            return links;
        }

        public int GetPageCount()
        {
            int result = 1;
            if (doc.GetElementbyId("SearchResultsHeading") != null)
            {

                return int.Parse(Regex.Match(doc.GetElementbyId("SearchResultsHeading").InnerText, @"\d+").Value) / 30;
            }
            return result;
        }
    }
}
